#ifndef SOURCE_H
#define SOURCE_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <regex.h>
typedef struct source {
  int current_pos;
  int current_line;
  int buffer_length;
  char *buffer;
  FILE *f;
} Source;
Source *source_create(int current_pos, int current_line, int buffer_length, FILE *f);
char source_current_char(Source *source);
char source_next_char(Source *source);
char source_peek_char(Source *source);
void source_destroy(Source **this);
#endif
