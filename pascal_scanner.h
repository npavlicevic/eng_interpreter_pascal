#ifndef PASCAL_SCANNER_H
#define PASCAL_SCANNER_H
#include "source.h"
typedef enum pascal_type {IS_WORD = 128, IS_INTEGER, IS_REAL, IS_STRING, IS_SPECIAL_CHAR, IS_ERROR, FALSE, TRUE} PascalType;
// TODO move enums to common file
typedef struct pascal_token {
  void *value;
  // for all numeric values
  PascalType type;
  int string_length;
  int buffer_length;
  char *buffer;
  // for all string values
} PascalToken;
PascalToken *pascal_token_create(int buffer_length);
int pascal_extract_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*), char (*source_peek_char)(Source*));
char pascal_skip_whitespace(Source *source, char (*source_next_char)(Source*));
int pascal_word_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*));
int pascal_number_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*), char (*source_peek_char)(Source*));
int pascal_string_token(PascalToken *this, Source *source, char (*source_next_char)(Source*), char (*source_peek_char)(Source*));
int pascal_special_char_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*));
char pascal_is_char(int ch);
void pascal_token_destroy(PascalToken **this);
#endif
