#include "pascal_scanner_test.h"

int main() {
  pascal_scanner_test();
  pascal_scanner_another_test();
  pascal_scanner_string_test();
  pascal_scanner_number_test();
  pascal_is_char_test();
  pascal_scanner_char_test();
  return 0;
}
