#include "source.h"
Source *source_create(int current_pos, int current_line, int buffer_length, FILE *f) {
  Source *this = (Source*)calloc(1, sizeof(Source));
  this->current_pos = current_pos;
  this->current_line = current_line;
  this->buffer_length = buffer_length;
  this->buffer = (char*)calloc(this->buffer_length, sizeof(char));
  this->f = f;
  return this;
}
char source_current_char(Source *this) {
  char *count;
  if(this->current_pos && this->current_pos == (int)strlen(this->buffer)) {
    return '\n';
  } else if(this->current_pos && this->current_pos > (int)strlen(this->buffer)) {
    this->current_pos = -1;
    this->current_line += 1;
    count = fgets(this->buffer, this->buffer_length, this->f);
    if(count == NULL) {
      memset(this->buffer, 0, this->buffer_length);
    }
    return source_next_char(this);
  } else if (!strlen(this->buffer)) {
    return EOF;
  } else {
    return this->buffer[this->current_pos];
  }
}
char source_next_char(Source *this) {
  this->current_pos++;
  return source_current_char(this);
}
char source_peek_char(Source *this) {
  char _r;
  if(this->current_pos + 1 < (int)strlen(this->buffer)) {
    _r = this->buffer[this->current_pos + 1];
  } else {
    _r = source_current_char(this);
  }
  return _r;
}
void source_destroy(Source **this) {
  fclose((*this)->f);
  free((*this)->buffer);
  (*this)->buffer = NULL;
  free(*this);
  *this = NULL;
}
