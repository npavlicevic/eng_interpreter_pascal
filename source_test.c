#include "source_test.h"
void source_test() {
  Source *source = source_create(1, 0, 256, fopen("hello.pas", "r"));
  char ch;
  while((ch = source_next_char(source)) != EOF) {
    printf("%c", ch);
  }
  printf("\n");
  source_destroy(&source);
}
