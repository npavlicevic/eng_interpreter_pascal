#ifndef PASCAL_SCANNER_TEST
#define PASCAL_SCANNER_TEST
#include <assert.h>
#include "pascal_scanner.h"
void pascal_scanner_test();
void pascal_scanner_another_test();
void pascal_scanner_string_test();
void pascal_scanner_number_test();
void pascal_is_char_test();
void pascal_scanner_char_test();
#endif
