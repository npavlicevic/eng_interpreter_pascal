#include "pascal_scanner_test.h"
void pascal_scanner_test() {
  int _r;
  PascalToken *pascal_token = pascal_token_create(256);
  Source *source = source_create(1, 0, 256, fopen("hello.pas", "r"));
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  assert(_r);
  assert(pascal_token->string_length);
  source_destroy(&source);
  pascal_token_destroy(&pascal_token);
}
void pascal_scanner_another_test() {
  int _r;
  PascalToken *pascal_token = pascal_token_create(256);
  Source *source = source_create(1, 0, 256, fopen("scannertest.txt", "r"));
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  assert(_r);
  assert(pascal_token->string_length);
  source_destroy(&source);
  pascal_token_destroy(&pascal_token);
}
void pascal_scanner_string_test() {
  int _r;
  PascalToken *pascal_token = pascal_token_create(256);
  Source *source = source_create(1, 0, 256, fopen("scannertest.txt", "r"));
  while(pascal_token->type != IS_STRING) {
    _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  }
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  // TODO check if print same as in other example
  assert(pascal_token->type == IS_STRING);
  assert(_r);
  source_destroy(&source);
  pascal_token_destroy(&pascal_token);
}
void pascal_scanner_number_test() {
  int _r;
  PascalToken *pascal_token = pascal_token_create(256);
  Source *source = source_create(1, 0, 256, fopen("scannertest.txt", "r"));
  while(pascal_token->type != IS_INTEGER && pascal_token->type != IS_REAL) {
    _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  }
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  assert(pascal_token->type == IS_INTEGER || pascal_token->type == IS_REAL);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  assert(pascal_token->type == IS_ERROR);
  printf("IS_ERROR %f %s column %d line %d\n", *(double*)pascal_token->value, pascal_token->buffer, source->current_pos, source->current_line);
  assert(_r);
  source_destroy(&source);
  pascal_token_destroy(&pascal_token);
}
void pascal_scanner_char_test() {
  int _r;
  PascalToken *pascal_token = pascal_token_create(256);
  Source *source = source_create(1, 0, 256, fopen("scannertest.txt", "r"));
  while(pascal_token->type != IS_SPECIAL_CHAR) {
    _r = pascal_extract_token(pascal_token, source, source_current_char, source_next_char, source_peek_char);
  }
  assert(pascal_token->type = IS_SPECIAL_CHAR);
  assert(_r);
  source_destroy(&source);
  pascal_token_destroy(&pascal_token);
}
void pascal_is_char_test() {
  assert(pascal_is_char(47));
  assert(!pascal_is_char(48));
}
