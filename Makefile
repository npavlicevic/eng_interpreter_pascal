#
# Makefile 
# pascal interpreter
#

CC=gcc
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic

FILES=source.c source_test.c main.c
FILES_PASCAL_SCANNER=source.c pascal_scanner.c pascal_scanner_test.c main_pascal_scanner.c
CLEAN=main main_pascal_scanner

all: main main_pascal_scanner

main: ${FILES}
	${CC} ${CFLAGS} $^ -o main 

main_pascal_scanner: ${FILES_PASCAL_SCANNER}
	${CC} ${CFLAGS} $^ -o main_pascal_scanner 

clean: ${CLEAN}
	rm $^
