#include "pascal_scanner.h"
PascalToken *pascal_token_create(int buffer_length) {
  PascalToken *this = (PascalToken*)calloc(1, sizeof(PascalToken));
  this->value = NULL;
  this->string_length = 0;
  this->buffer_length = buffer_length;
  this->buffer = (char*)calloc(this->buffer_length, sizeof(char));
  return this;
}
int pascal_extract_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*), char (*source_peek_char)(Source*)) {
  int _r;
  char ch = pascal_skip_whitespace(source, source_next_char);
  if(ch == EOF) {
    printf("eof\n");
    _r = 0;
  } else if(isalpha(ch)) {
    _r = pascal_word_token(this, source, source_current_char, source_next_char);
    printf("letter %s\n", this->buffer);
  } else if(isdigit(ch)) {
    _r = pascal_number_token(this, source, source_current_char, source_next_char, source_peek_char);
    (this->type == IS_INTEGER) ? printf("number %d\n", *(int*)this->value) : printf("number %f\n", *(double*)this->value);
  } else if(ch == '\'') {
    _r = pascal_string_token(this, source, source_next_char, source_peek_char);
    printf("string %s\n", this->buffer);
  } else if(pascal_is_char(ch)) {
    _r = pascal_special_char_token(this, source, source_current_char, source_next_char);
    printf("special char %s\n", this->buffer);
  }
  return _r;
  // TODO remove printf
  // TODO add error checking
  // TODO special symbols
}
char pascal_skip_whitespace(Source *source, char (*source_next_char)(Source*)) {
  char ch = source_next_char(source);
  while(isspace(ch) || ch == '{') {
    if(ch == '{') {
      while(ch != '}') {
        ch = source_next_char(source);
      }
    }
    ch = source_next_char(source);
  }
  return ch;
}
int pascal_word_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*)) {
  // extract a keyword
  int _r = 0;
  char ch = source_current_char(source);
  while(isalpha(ch)) {
    this->buffer[_r++] = ch;
    ch = source_next_char(source);
  }
  this->buffer[_r] = 0;
  this->string_length = _r;
  this->type = IS_WORD;
  return _r;
}
int pascal_number_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*), char (*source_peek_char)(Source*)) {
  // extract a number
  this->type = IS_INTEGER;
  int _r = 0;
  int *value;
  double *value_double;
  int regex_r;
  regex_t regex_int;
  regex_r = regcomp(&regex_int, "[[:digit:]]+", REG_EXTENDED);
  regex_t regex_double;
  regex_r = regcomp(&regex_double, "^[[:digit:]]+\\.[[:digit:]]+$", REG_EXTENDED);
  regex_t regex_exp;
  regex_r = regcomp(&regex_exp, "[[:digit:]]+\\.?[[:digit:]]+[eE][\\+-]?[[:digit:]]+", REG_EXTENDED);
  PascalType dot_dot = FALSE;
  char ch = source_current_char(source);
  while(isdigit(ch)) {
    this->buffer[_r++] = ch;
    ch = source_next_char(source);
  }
  if(ch == '.') {
    if(source_peek_char(source) == '.') {
      // dot dot ..
      dot_dot = TRUE;
    } else {
      this->buffer[_r++] = ch;
      this->type = IS_REAL;
      while((ch = source_next_char(source)) && isdigit(ch)) {
        this->buffer[_r++] = ch;
      }
    }
  }
  if(dot_dot == FALSE && (ch == 'E' || ch == 'e')) {
    this->type = IS_REAL;
    this->buffer[_r++] = ch;
    ch = source_next_char(source);
    if((ch == '+') || (ch == '-')) {
      this->buffer[_r++] = ch;
      ch = source_next_char(source);
    }
    while(isdigit(ch)) {
      this->buffer[_r++] = ch;
      ch = source_next_char(source);
    }
  }
  this->buffer[_r] = 0;
  this->string_length = _r;
  if(this->value != NULL) {
    free(this->value);
    this->value = NULL;
  }  
  if(this->type == IS_INTEGER) {
    value = (int*)calloc(1, sizeof(int));
    *value = atoi(this->buffer);
    this->value = value;
    regex_r = regexec(&regex_int, this->buffer, 0, NULL, 0);
    if(regex_r) {
      this->type = IS_ERROR;
    }
  } else {
    value_double = (double*)calloc(1, sizeof(double));
    *value_double = atof(this->buffer);
    this->value = value_double;
    regex_r = regexec(&regex_double, this->buffer, 0, NULL, 0) && regexec(&regex_exp, this->buffer, 0, NULL, 0);
    if(regex_r) {
      this->type = IS_ERROR;
    }
  }
  regfree(&regex_int);
  regfree(&regex_double);
  regfree(&regex_exp);
  return _r;
}
int pascal_string_token(PascalToken *this, Source *source, char (*source_next_char)(Source*), char (*source_peek_char)(Source*)) {
  // extract string in quotes
  int _r = 0;
  char ch = source_next_char(source);
  this->buffer[_r++] = '\'';
  do {
    if(isspace(ch)) {
      // just space, no other alternative character
      ch = ' ';
    }
    if(ch != '\'' && ch != EOF) {
      this->buffer[_r++] = ch;
      ch = source_next_char(source);
    }
    if(ch == '\'') {
      while(ch == '\'' && source_peek_char(source) == '\'') {
        this->buffer[_r++] = '\'';
        this->buffer[_r++] = '\'';
        ch = source_next_char(source);
        ch = source_next_char(source);
      }
    }
  } while(ch != '\'' && ch != EOF);
  this->buffer[_r] = '\'';
  this->buffer[++_r] = 0;
  this->string_length = _r;
  this->type = IS_STRING;
  return _r;
}
int pascal_special_char_token(PascalToken *this, Source *source, char (*source_current_char)(Source*), char (*source_next_char)(Source*)) {
  int _r = 0;
  char ch = source_current_char(source);
  this->buffer[_r++] = ch;
  switch(ch) {
    case '+': case '-': case '*': case '/': case ',': case ';': case '=': case '(': case ')':
    case '[': case ']': case '{': case '}': case '^': {
      ch = source_next_char(source);
      break;
    }
    case ':': {
      ch = source_next_char(source);
      if(ch == '=') {
        this->buffer[_r++] = ch;
        ch = source_next_char(source);
      }
      break;
    }
    case '<': {
      ch = source_next_char(source);
      if(ch == '=') {
        this->buffer[_r++] = ch;
        ch = source_next_char(source);
      } else if(ch == '>') {
        this->buffer[_r++] = ch;
        ch = source_next_char(source);
      }
      break;
    }
    case '>': {
      ch = source_next_char(source);
      if(ch == '=') {
        this->buffer[_r++] = ch;
        ch = source_next_char(source);
      }
      break;
    }
    case '.': {
      ch = source_next_char(source);
      if(ch == '.') {
        this->buffer[_r++] = ch;
        ch = source_next_char(source);
      }
      break;
    }
    default: {
      ch = source_next_char(source);
      this->type = IS_ERROR;
    }
  }
  this->buffer[_r] = 0;
  this->string_length = _r;
  this->type = IS_SPECIAL_CHAR;
  return _r;
}
char pascal_is_char(int ch) {
  char pascal_char[128] = {[39] = 1, [40] = 1, [41] = 1, [42] = 1, [43] = 1, [44] = 1, [45] = 1, [46] = 1, [47] = 1, [58] = 1, [59] = 1, [60] = 1, [61] = 1, [62] = 1, [91] = 1, [93] = 1, [94] = 1, [123] = 1, [125] = 1};
  return pascal_char[ch];
}
void pascal_token_destroy(PascalToken **this) {
  free((*this)->buffer);
  (*this)->buffer = NULL;
  free(*this);
  *this = NULL;
}
